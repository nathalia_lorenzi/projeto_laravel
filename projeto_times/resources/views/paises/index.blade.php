@extends('layouts.default')

@section('content')
    <h1>Paises</h1>
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($paises as $pais)
                <tr>
                    <td>{{ $pais->nome }}</td>
                    <td>
                        <a href="{{ route('paises.edit', ['id'=>$pais->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmarExclusao({{$pais->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>    
            @endforeach
        </tbody>
    </table>

    {{ $paises->links("pagination::bootstrap-4") }}

    <a href="{{ route('paises.create', []) }}" class="btn-sm btn-info">Adicionar</a>
@stop

@section('table-delete')
"paises"
@endsection