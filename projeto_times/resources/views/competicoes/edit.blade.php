@extends('adminlte::page')

@section('content')
    <h3>Editando Competicao: {{ $competicao->nome }}</h3>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::open(['route'=> ["competicoes.update", 'id'=>$competicao->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('nome', 'Nome:') !!}
            {!! Form::text('nome', $competicao->nome, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('temporada', 'Temporada:') !!}
            {!! Form::text('temporada', $competicao->temporada, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Editar Pais', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar',['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop