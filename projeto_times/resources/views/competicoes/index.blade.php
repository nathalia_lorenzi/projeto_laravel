@extends('layouts.default')

@section('content')
    <h1>Competições</h1>
    {!! Form::open(['name'=>'form_name', 'route'=>'competicoes']) !!}
    <div class="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;" placeholder="Pesquisa...">
            <span class="input-group-btn">
                <button types="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Temporada</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($competicoes as $competicao)
                <tr>
                    <td>{{ $competicao->nome }}</td>
                    <td>{{ $competicao->temporada }}</td>
                    <td>
                        <a href="{{ route('competicoes.edit', ['id'=>$competicao->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmarExclusao({{$competicao->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>    
            @endforeach
        </tbody>
    </table>

    {{ $competicoes->links("pagination::bootstrap-4") }}

    <a href="{{ route('competicoes.create', []) }}" class="btn-sm btn-info">Adicionar</a>
@stop

@section('table-delete')
"competicoes"
@endsection