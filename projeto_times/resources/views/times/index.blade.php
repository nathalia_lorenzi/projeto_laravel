@extends('layouts.default')

@section('content')
    <h1>Times</h1>
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Técnico</th>
            <th>Pais</th>
            <th>Estadio</th>
            <th>Ano de Fundação</th>
            <th>Competições</th>

        </thead>

        <tbody>
            @foreach($times as $time)
                <tr>
                    <td>{{ $time->nome}}</td>
                    <td>{{ $time->tecnico->nome}}</td>
                    <td>{{ $time->pais->nome}}</td>
                    <td>{{ $time->estadio}}</td>
                    <td>{{ $time->ano_fundacao}}</td>
                    <td>
                        @foreach ($time->competicoes as $c)
                            <li>{{ $c->competicao->nome }}</li>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $times->links("pagination::bootstrap-4") }}

    <a href="{{ route('times.create', []) }}" class="btn btn-info">Adicionar</a>
@stop

@section('table-delete')
"times"
@endsection
