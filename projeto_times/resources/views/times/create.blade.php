@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-header" style="background: lightgrey">
            
        </div>
        <br>

        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">
            {!! Form::open(['route' => 'times.store']) !!}

                <div class="form_group">
                    {!! Form::label('nome', 'Nome:') !!}
                    {!! Form::text('nome', null, ['class' => 'form-control', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('pais_id', 'Pais:') !!}
                    {!! Form::select('pais_id', 
                                    \App\Models\Pais::orderBy('nome')->pluck('nome', 'id')->toArray(), 
                                    null, ['class'=>'form-control', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('tecnico_id', 'Técnico:') !!}
                    {!! Form::select('tecnico_id', 
                                    \App\Models\Tecnico::orderBy('nome')->pluck('nome', 'id')->toArray(), 
                                    null, ['class'=>'form-control', 'required']) !!}
                </div>

                <div class="form_group">
                    {!! Form::label('estadio', 'Estadio:') !!}
                    {!! Form::text('estadio', null, ['class' => 'form-control', 'required']) !!}
                </div>

                <div class="form_group">
                    {!! Form::label('ano_fundacao', 'Ano de Fundação:') !!}
                    {!! Form::date('ano_fundacao', null, ['class' => 'form-control', 'required']) !!}
                </div>
                <hr />

                <h4>Competições</h4>
                <div class="input_fields_wrap"></div>
                <br>

                <button style="float: right" class="add_field_button btn btn-default">Adicionar Competição</button>
                
                <br>
                <hr />

                <div class="form-group">
                    {!! Form::submit('Criar Time', ['class'=>'btn btn-primary']) !!}
                </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function(){
            var wrapper = $(".input_fields_wrap");
            var add_button = $(".add_field_button");
            var x=0;
            $(add_button).click(function(e){
                x++;
                var newField = '<div><div style="width:94%; float:left" id="competicao">{!! Form::select("competicoes[]", \App\Models\Competicao::orderBy("nome")->pluck("nome", "id")->toArray(), null, ["class"=>"form-control", "required", "placeholder"=>"Selecione uma Competição"]) !!} </div><button type="button" class="remove_field btn btn-danger btn-circle"><i class="fa fa-times"></button></div>';
                $(wrapper).append(newField);
            });
            $(wrapper).on("click", ".remove_field", function(e){
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            });
        })
    </script>
@stop