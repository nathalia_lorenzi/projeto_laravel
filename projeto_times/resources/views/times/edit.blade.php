@extends('adminlte::page')

@section('content')
    <h3>Editando Time: {{ $time->nome}} </h3>
    <br>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif


    {!! Form::open(['route' => ["times.update", 'id'=>$time->id], 'method'=>'put']) !!}

        <div class="form_group">
            {!! Form::label('nome', 'Nome:') !!}
            {!! Form::text('nome', $time->nome, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form_group">
            {!! Form::label('pais', 'Pais:') !!}
            {!! Form::text('pais', $time->pais, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form_group">
            {!! Form::label('tecnico', 'Técnico:') !!}
            {!! Form::text('tecnico', $time->tecnico, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form_group">
            {!! Form::label('estadio', 'Estadio:') !!}
            {!! Form::text('estadio', $time->estadio, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form_group">
            {!! Form::label('ano_fundacao', 'Ano de fundação:') !!}
            {!! Form::date('ano_fundacao', $time->ano_fundacao, ['class' => 'form-control', 'required']) !!}
        </div>
        <br>

        <div class="form_group">
            {!! Form::submit('Editar Time', ['class' => 'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class' => 'btn btn-default']) !!}
        </div>

    {!! Form::close() !!}
@stop