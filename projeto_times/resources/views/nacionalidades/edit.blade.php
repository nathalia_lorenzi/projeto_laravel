@extends('adminlte::page')

@section('content')
    <h3>Editando Nacionalidade: {{ $nacionalidade->nome}} </h3>
    <br>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::open(['route' => ["nacionalidades.update", 'id'=>$nacionalidade->id], 'method'=>'put']) !!}

        <div class="form_group">
            {!! Form::label('descricao', 'Descrição:') !!}
            {!! Form::text('descricao', $nacionalidade->descricao, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form_group">
            {!! Form::submit('Editar Nacionalidadde', ['class' => 'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class' => 'btn btn-default']) !!}
        </div>

    {!! Form::close() !!}
@stop