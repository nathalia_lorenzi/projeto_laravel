@extends('adminlte::page')

@section('content')
    <h3>Nova Nacionalidade</h3>
    <br>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif


    {!! Form::open(['route' => 'nacionalidades.store']) !!}

        <div class="form_group">
            {!! Form::label('descricao', 'Descrição:') !!}
            {!! Form::text('descricao', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="form_group">
            {!! Form::submit('Criar Nacionalidade', ['class' => 'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class' => 'btn btn-default']) !!}
        </div>

    {!! Form::close() !!}
@stop