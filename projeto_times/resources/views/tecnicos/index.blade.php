@extends('layouts.default')

@section('content')
    <h1>Tecnicos</h1>

    {!! Form::open(['name'=>'form_name', 'route'=>'tecnicos']) !!}

    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Data Nascimento</th>
            <th>Nacionalidade</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($tecnicos as $tecnico)
                <tr>
                    <td>{{ $tecnico->nome }}</td>
                    <td>{{ $tecnico->data_nascimento }}</td>
                    <td>{{ $tecnico->nacionalidade->descricao }}</td>
                    <td>
                        <a href="{{ route('tecnicos.edit', ['id'=>$tecnico->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmarExclusao({{$tecnico->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>    
            @endforeach
        </tbody>
    </table>

    {{ $tecnicos->links("pagination::bootstrap-4") }}

    <a href="{{ route('tecnicos.create', []) }}" class="btn-sm btn-info">Adicionar</a>
@stop

@section('table-delete')
"tecnicos"
@endsection