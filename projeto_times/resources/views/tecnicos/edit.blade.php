@extends('adminlte::page')

@section('content')
    <h3>Editando Tecnico: {{ $tecnico->nome }}</h3>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::open(['route'=> ["tecnicos.update", 'id'=>$tecnico->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('nome', 'Nome:') !!}
            {!! Form::text('nome', $tecnico->nome, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('data_nascimento', 'Data de Nascimento:') !!}
            {!! Form::date('data_nascimento', $tecnico->data_nascimento, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('nacionalidade_id', 'Nacionalidade:') !!}
            {!! Form::select('nacionalidade_id', 
                \App\Models\Nacionalidade::orderBy('descricao')->pluck('descricao', 'id')->toArray(),
                            $tecnico->nacionalidade_id, 
                            ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Editar Tecnico', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar',['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop