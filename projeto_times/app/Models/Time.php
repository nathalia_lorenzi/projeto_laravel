<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $table = "times";
    protected $fillable = ['nome', 'pais_id', 'tecnico_id', 'estadio', 'ano_fundacao'];

    public function pais() {
        return $this->belongsTo("App\Models\Pais");
    }

    public function tecnico() {
        return $this->belongsTo("App\Models\Tecnico");
    }

    public function competicoes() {
        return $this->hasMany("App\Models\CompeticaoTime");
    }
}
