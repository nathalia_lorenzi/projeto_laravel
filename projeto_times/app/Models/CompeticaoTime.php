<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompeticaoTime extends Model
{
    use HasFactory;

    protected $table = "competicao_times";
    protected $fillable = ['time_id', 'competicao_id'];

    public function competicao() {
        return $this->belongsTo("App\Models\Competicao");
    }
}
