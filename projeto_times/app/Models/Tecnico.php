<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tecnico extends Model
{
    use HasFactory;

    protected $table = "tecnicos";
    protected $fillable = ['nome', 'data_nascimento', 'nacionalidade_id'];

    public function nacionalidade() {
        return $this->belongsTo("App\Models\Nacionalidade");
    }
}
