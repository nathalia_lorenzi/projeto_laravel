<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Competicao extends Model
{
    use HasFactory;
    protected $table = "competicaos";
    protected $fillable = ['nome', 'temporada'];
}
