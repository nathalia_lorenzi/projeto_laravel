<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Time;
use App\Models\CompeticaoTime;
use App\Http\Requests\TimeRequest;

class TimesController extends Controller
{
    public function index() {
        $times = Time::orderBy('nome')->paginate(5);
        return view('times.index', ['times' =>$times]);
    }
    public function create() {
        return view('times.create');
    }

    public function store(TimeRequest $request) {
        $time = Time::create([
            'nome' => $request->get('nome'),
            'tecnico_id' => $request->get('tecnico_id'), 
            'pais_id' => $request->get('pais_id'),
            'estadio' => $request->get('estadio'),
            'ano_fundacao' => $request->get('ano_fundacao')
        ]);
        $competicoes = $request->competicoes;
        foreach($competicoes as $c => $value) {
            CompeticaoTime::create([
                'time_id' => $time->id,
                'competicao_id' => $competicoes[$c]
            ]);
        }
        return redirect()->route('times');
    }

    public function destroy($id) {
        Time::find($id)->delete();
        return redirect()->route('times');
    }

    public function edit($id) {
        $time = Time::find($id);
        return view('times.edit', compact('time'));
    }

    public function update(TimeRequest $request, $id) {
        Time::find($id)->update($request->all());
        return redirect()->route('times');
    }
}
