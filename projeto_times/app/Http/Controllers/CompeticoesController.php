<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Competicao;

class CompeticoesController extends Controller
{

    public function index(Request $filtro) {
        $filtragem = $filtro->get('desc_filtro');
        if($filtragem == null)
            $competicoes = Competicao::orderBy('nome')->paginate(5);
        else
            $competicoes = Competicao::where('nome', 'like', '%'.$filtragem.'%')
                                ->orderBy("nome")
                                ->paginate(5)
                                ->setPath('competicoes?desc_filtro='.$filtragem);

        return view('competicoes.index', ['competicoes'=>$competicoes]);
    }

    public function create() {
        return view('competicoes.create');
    }

    public function store(Request $request){
        $nova_competicao= $request->all();
        Competicao::create($nova_competicao);

        return redirect()->route('competicoes');
    }

    public function destroy($id) {
        Competicao::find($id)->delete();
        return redirect()->route('competicoes');
    }

    public function edit($id) {
        $competicao = Competicao::find($id);
        return view('competicoes.edit', compact('competicao'));
    }

    public function update(Request $request, $id){
        Competicao::find($id)->update($request->all());
        return redirect()->route('competicoes');
    }
}
