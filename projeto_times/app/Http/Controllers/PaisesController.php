<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pais;

class PaisesController extends Controller
{
    public function index() {
        $paises = Pais::orderBy('nome')->paginate(5);
        return view('paises.index', ['paises'=>$paises]);
    }

    public function create() {
        return view('paises.create');
    }

    public function store(Request $request){
        $novo_pais= $request->all();
        Pais::create($novo_pais);

        return redirect()->route('paises');
    }

    public function destroy($id) {
        try{ 
            Pais::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>'null');
        } catch (\Illuminate\Database\QueryException $e) {
            $ret = array('status'=>500, 'msg'=> $e->getMessage());
        }catch(\PDOException $e) {
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        return $ret;
    }

    public function edit($id) {
        $pais = Pais::find($id);
        return view('paises.edit', compact('pais'));
    }

    public function update(Request $request, $id){
        Pais::find($id)->update($request->all());
        return redirect()->route('paises');
    }
}
