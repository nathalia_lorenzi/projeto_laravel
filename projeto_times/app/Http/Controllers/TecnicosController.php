<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tecnico;

class TecnicosController extends Controller
{
    public function index() {
        $tecnicos = Tecnico::orderBy('nome')->paginate(5);
        return view('tecnicos.index', ['tecnicos'=>$tecnicos]);
    }

    public function create() {
        return view('tecnicos.create');
    }

    public function store(Request $request){
        $novo_tecnico = $request->all();
        Tecnico::create($novo_tecnico);

        return redirect()->route('tecnicos');
    }

    public function destroy($id) {
        try{ 
            Tecnico::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>'null');
        } catch (\Illuminate\Database\QueryException $e) {
            $ret = array('status'=>500, 'msg'=> $e->getMessage());
        }catch(\PDOException $e) {
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        return $ret;
    }

    public function edit($id) {
        $tecnico = Tecnico::find($id);
        return view('tecnicos.edit', compact('tecnico'));
    }

    public function update(Request $request, $id){
        Tecnico::find($id)->update($request->all());
        return redirect()->route('tecnicos');
    }
}
