<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function() {
    Route::group(['prefix'=>'times', 'where'=>['id'=>'[0-9]+']], function() {
        Route::get('',             ['as'=>'times',   'uses'=>'App\Http\Controllers\TimesController@index']);
        Route::get('create',       ['as'=>'times.create',  'uses'=>'App\Http\Controllers\TimesController@create']);
        Route::post('store',       ['as'=>'times.store',   'uses'=>'App\Http\Controllers\TimesController@store']);
        Route::get('{id}/destroy', ['as'=>'times.destroy', 'uses'=>'App\Http\Controllers\TimesController@destroy']);
        Route::get('{id}/edit',    ['as'=>'times.edit',    'uses'=>'App\Http\Controllers\TimesController@edit']);
        Route::put('{id}/update',  ['as'=>'times.update',  'uses'=>'App\Http\Controllers\TimesController@update']);
    });

    Route::group(['prefix'=>'nacionalidades', 'where'=>['id'=>'[0-9]+']], function() {
        Route::get('',             ['as'=>'nacionalidades',   'uses'=>'App\Http\Controllers\NacionalidadesController@index']);
        Route::get('create',       ['as'=>'nacionalidades.create',  'uses'=>'App\Http\Controllers\NacionalidadesController@create']);
        Route::post('store',       ['as'=>'nacionalidades.store',   'uses'=>'App\Http\Controllers\NacionalidadesController@store']);
        Route::get('{id}/destroy', ['as'=>'nacionalidades.destroy', 'uses'=>'App\Http\Controllers\NacionalidadesController@destroy']);
        Route::get('{id}/edit',    ['as'=>'nacionalidades.edit',    'uses'=>'App\Http\Controllers\NacionalidadesController@edit']);
        Route::put('{id}/update',  ['as'=>'nacionalidades.update',  'uses'=>'App\Http\Controllers\NacionalidadesController@update']);
    });

    Route::group(['prefix'=>'tecnicos', 'where'=>['id'=>'[0-9]+']], function() {
        Route::get('',             ['as'=>'tecnicos',   'uses'=>'App\Http\Controllers\TecnicosController@index']);
        Route::get('create',       ['as'=>'tecnicos.create',  'uses'=>'App\Http\Controllers\TecnicosController@create']);
        Route::post('store',       ['as'=>'tecnicos.store',   'uses'=>'App\Http\Controllers\TecnicosController@store']);
        Route::get('{id}/destroy', ['as'=>'tecnicos.destroy', 'uses'=>'App\Http\Controllers\TecnicosController@destroy']);
        Route::get('{id}/edit',    ['as'=>'tecnicos.edit',    'uses'=>'App\Http\Controllers\TecnicosController@edit']);
        Route::put('{id}/update',  ['as'=>'tecnicos.update',  'uses'=>'App\Http\Controllers\TecnicosController@update']);
    });

    Route::group(['prefix'=>'paises', 'where'=>['id'=>'[0-9]+']], function() {
        Route::get('',             ['as'=>'paises',   'uses'=>'App\Http\Controllers\PaisesController@index']);
        Route::get('create',       ['as'=>'paises.create',  'uses'=>'App\Http\Controllers\PaisesController@create']);
        Route::post('store',       ['as'=>'paises.store',   'uses'=>'App\Http\Controllers\PaisesController@store']);
        Route::get('{id}/destroy', ['as'=>'paises.destroy', 'uses'=>'App\Http\Controllers\PaisesController@destroy']);
        Route::get('{id}/edit',    ['as'=>'paises.edit',    'uses'=>'App\Http\Controllers\PaisesController@edit']);
        Route::put('{id}/update',  ['as'=>'paises.update',  'uses'=>'App\Http\Controllers\PaisesController@update']);
    });

    Route::group(['prefix'=>'competicoes', 'where'=>['id'=>'[0-9]+']], function() {
        Route::any('',             ['as'=>'competicoes',   'uses'=>'App\Http\Controllers\CompeticoesController@index']);
        Route::get('create',       ['as'=>'competicoes.create',  'uses'=>'App\Http\Controllers\CompeticoesController@create']);
        Route::post('store',       ['as'=>'competicoes.store',   'uses'=>'App\Http\Controllers\CompeticoesController@store']);
        Route::get('{id}/destroy', ['as'=>'competicoes.destroy', 'uses'=>'App\Http\Controllers\CompeticoesController@destroy']);
        Route::get('{id}/edit',    ['as'=>'competicoes.edit',    'uses'=>'App\Http\Controllers\CompeticoesController@edit']);
        Route::put('{id}/update',  ['as'=>'competicoes.update',  'uses'=>'App\Http\Controllers\CompeticoesController@update']);
    });

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
